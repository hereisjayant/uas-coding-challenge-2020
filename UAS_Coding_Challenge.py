# for vector operations
from numpy import array
import math

def getIntersections(x1, y1, x2, y2, x0, y0, r):
    #Defining a circle:
    C_circle = array([x0, y0])
    radius = r

    #Defining a line:

# P1_line + t.V_line
# Point on the line: (x1 + t.v1, y1 + t.v1)
    P1_line = array([x1, y1])
    P2_line = array([x2, y2])
    #vector joining P1 and P2
    V_line = P2_line - P1_line

# centre is equal to radius when
# (X-C_circle) = radius
#=P1_line+t.V_line-C_circle = radius
# Squaring both sides:
# (P1_line+t.V_line-C_circle)**2 = r**2

# Now solving the roots Ax^2+Bx+C = 0:

    A = V_line.dot(V_line)
    B = 2 * V_line.dot(P1_line - C_circle)
    C = P1_line.dot(P1_line) + C_circle.dot(C_circle) - 2 * P1_line.dot    (C_circle) - radius**2

    d = B**2 - 4 * A * C

    if(d<0):
        print("\nNo Intersections")

    elif(d==0):
        print("\nOne intersection")
        sqrt_d = math.sqrt(d)
        #Solutions:
        t1 = (-B + sqrt_d) / (2 * A)
        sol1 = P1_line+ t1*V_line
        print("\n", sol1)

    else:
        sqrt_d = math.sqrt(d)
        #Solutions:
        t1 = (-B + sqrt_d) / (2 * A)
        t2 = (-B - sqrt_d) / (2 * A)
        sol1 = P1_line+ t1*V_line
        sol2 = P1_line+ t2*V_line
        print("\nTwo Intersections")
        print("\n", sol1, sol2)


#Test case 1:
print("\nTest 1:")
x1 = 0
y1 = -10
x2 = 15
y2 = 15
x0 = 9
y0 = 3
radius = 5
getIntersections(x1, y1, x2, y2, x0, y0, radius)

#Test case 2:
print("\nTest 2:")
x1 = 0
y1 = -10
x2 = 15
y2 = 15
x0 = 10
y0 = -5
radius = 4
getIntersections(x1, y1, x2, y2, x0, y0, radius)

#Test case 3:
print("\nTest 3:")
x1 = 0
y1 = 10
x2 = 30
y2 = 10
x0 = 12
y0 = 0
radius = 10
getIntersections(x1, y1, x2, y2, x0, y0, radius)
